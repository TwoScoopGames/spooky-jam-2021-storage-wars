using Random = UnityEngine.Random;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Bidder {
  public string name;
  public int totalGoldPieces;
  public int willingToPayGoldPieces;

  public string BasicDescription {
    get {
      return string.Format("{0} (has {1} gp / willing to bid {2} gp)", name, totalGoldPieces, willingToPayGoldPieces);
    }
  }

  public static Bidder Generate(int actualWorthGoldPieces, int playersGoldPieces) {
    var range = Random.Range(0f, 1f);
    int min = actualWorthGoldPieces - Mathf.RoundToInt(actualWorthGoldPieces * range);
    int max = actualWorthGoldPieces + Mathf.RoundToInt(actualWorthGoldPieces * range);
    int guessAtWorth = Random.Range(min, max);
    var maxDiscount = Random.Range(-0.05f, 0.2f);
    int willingToPay = guessAtWorth - Mathf.RoundToInt(guessAtWorth * maxDiscount);

    int total = Random.Range((int)(playersGoldPieces / 2), (int)(playersGoldPieces * 10));

    return new Bidder {
      name = RandomName(),
      totalGoldPieces = total,
      willingToPayGoldPieces = Math.Min(willingToPay, total)
    };
  }

  private static readonly string[] namePrefixes = new[] {
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Sir",
    "Dr.",
    "Hon.",
    "Master",
    "Evil",
    "Invisible",
    "The ghost of",
    "Spicy",
    "Eloquent",

  };
  private static readonly string[] nameFirst = new [] {
    "Alex",
    "Augustus",
    "Barclay",
    "Cara",
    "Colton",
    "Erin",
    "Jack",
    "Josh",
    "Ruth",
    "Allen",
    "Loi",
    "Barry",
    "Stephen",
    "Eric",
    "Dee",
    "Rex",
    "Glenn",
    "Robert",
    "Link",
    "Cloud",
    "Princess",
    "Anola",
    "Larry",
    "Lorenzo",
    "Chungus",
    "Big",
    "Barbara",
    "Viggo",
    "Aragorn",
    "Gandalf",
    "Frodo",
    "Legolas",
    "Gollum",
    "Sauron",
    "Arwen",
    "Gimli",
    "Samwise",
    "Bilbo",
    "Galadriel",
    "Boromir",
    "Robb",
    "Ned",
    "Bran",
    "Jaime",
    "Tyrion",
    "Draenerys",
    "Sansa",
    "Cersei",
    "Arya",
    "Jon",

  "Ganon",
  "Agahnim",
  "Nightmares",
  "Dark",
  "Twinrova",
  "Majora",
  "Vaati",
  "Zant",
  "Demise",
    "Ghirahim",
    "Yuga",
    "Cia",
    "Volga",
    "Wizzro",
    "Yiga",
  "Kohga",
    "Astor",

  };
  private static readonly string[] nameLast = new [] {
    "Snow",
    "Arthur",
    "Brown",
    "Charles",
    "Cooper",
    "Smith",
    "Waldorf",
    "Essex",
    "The Dastardly",
    "The Stonk",
    "The Great",
    "of Westershire",
    "of Hyrule",
    "of Nibelheim",
    "of Narshe",
    "of Midgar",
    "Zorldo",
    "The Quick",
    "The Spicy",
    "The Radical",
    "Chungus",
    "Amoungus",
    "The Wise",
    "The Relentless",
    "of Biscuitworld",
    "Lee",
    "No Toast",
    "Longegg",
    "with a limp",
    "the Tall",
    "the Cutest",
    ", Town drunk",
    "Gamgee",
    "McKellen",
    "Bloom",
    "Baggins",
    "Bean",
    "Stark",
    "Lannister",
    "Targaryen",
    "with Eyes",

  };
  private static readonly string[] nameSuffixes = new [] {
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Jr.",
    "Sr.",
    "III"
  };

  private static string RandomName() {
    return string.Join(" ", new [] {
      Array.RandomElement(namePrefixes),
      Array.RandomElement(nameFirst),
      Array.RandomElement(nameLast),
      Array.RandomElement(nameSuffixes),
    }).Trim().Replace("  ", " ");
  }
}

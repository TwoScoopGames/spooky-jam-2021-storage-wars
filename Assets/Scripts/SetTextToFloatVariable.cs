using System.Collections;
using System.Collections.Generic;
using TMPro;
using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;

public class SetTextToFloatVariable : MonoBehaviour
{
  private TextMeshProUGUI text;
  public FloatReference variable;
  public string format = "F1";

  void Start() {
    text = GetComponent<TextMeshProUGUI>();
    text.text = variable.Value.ToString(format);
  }

  void Update() {
    text.text = variable.Value.ToString(format);
  }
}

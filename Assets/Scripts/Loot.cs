using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="ScriptableObjects/Loot")]
public class Loot : ScriptableObject {
  public string nameTemplate;
  public int numberInWorld; // Higher is more common
  public GameObject prefab;
  public float volumeCubicFeet;
  public float weightPounds;
  public int worthGoldPieces;

  private static readonly Dictionary<string, string[]> nameReplacements = new Dictionary<string, string[]>() {
    { "EffectSuffix", new[] { "of Strength", "of Knowing", "of Far Sight", "of Many Legs" } },
    { "KeyPrefix", new[] { "Rusty", "Iron", "Steel", "Gold" } }
  };

  private string GenerateName() {
    var n = nameTemplate;
    foreach (var replacement in nameReplacements) {
      n = n.Replace(string.Format("{{{0}}}", replacement.Key), Array.RandomElement(replacement.Value));
    }
    return n.Trim();
  }

  public LootInstance GenerateInstance() {
    return new LootInstance {
      name = GenerateName(),
      volumeCubicFeet = volumeCubicFeet,
      weightPounds = weightPounds,
      worthGoldPieces = worthGoldPieces,
      prefab = prefab
    };
  }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;

public class SetTextToIntVariable : MonoBehaviour
{
  private TextMeshProUGUI text;
  public IntReference variable;

  void Start() {
    text = GetComponent<TextMeshProUGUI>();
    text.text = variable.Value.ToString();
  }

  void Update() {
    text.text = variable.Value.ToString();
  }
}

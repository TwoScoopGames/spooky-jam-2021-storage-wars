using System.Collections.Generic;
using UnityEngine;

public class DeactivateAfterTime : MonoBehaviour {
  public float secondsUntilDeactivation;
  private float elapsed = 0;

  void Start() {
  }

  void Update() {
    elapsed += Time.deltaTime;
    if (elapsed >= secondsUntilDeactivation) {
      elapsed = 0;
      gameObject.SetActive(false);
    }
  }
}

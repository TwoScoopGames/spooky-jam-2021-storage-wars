using Random = UnityEngine.Random;
using System;
using System.Collections.Generic;
using UnityEngine;

public class LootGenerator : MonoBehaviour {
  public Loot[] loot;

  public void GenerateLoot() {
    new Auction(this, 1000);
  }

  public List<LootInstance> GenerateChest(float maxVolumeCubicFeet, float maxWeightPounds, int maxWorthGoldPieces) {
    var totalItemsInWorld = 0;
    foreach (var l in loot) {
      totalItemsInWorld += l.numberInWorld;
    }

    var items = new List<LootInstance>();

    float totalVolumeCubicFeet = 0;
    float totalWeightPounds = 0;
    int totalWorthGoldPieces = 0;
    do {
      var item = GenerateOne(totalItemsInWorld);
      items.Add(item);
      totalVolumeCubicFeet += item.volumeCubicFeet;
      totalWeightPounds += item.weightPounds;
      totalWorthGoldPieces += item.worthGoldPieces;
      Debug.LogFormat("{0}", item.BasicDescription);
    } while (
      totalVolumeCubicFeet < maxVolumeCubicFeet &&
      totalWeightPounds < maxWeightPounds &&
      totalWorthGoldPieces < maxWorthGoldPieces
    );

    Debug.LogFormat("{0} / {1} ft3 / {2} lbs / {3} gp", "Total", totalVolumeCubicFeet, totalWeightPounds, totalWorthGoldPieces);
    return items;
  }

  private LootInstance GenerateOne(int totalItemsInWorld) {
    var index = Random.Range(0, totalItemsInWorld);
    foreach (var l in loot) {
      if (index < l.numberInWorld) {
        return l.GenerateInstance();
      }
      index -= l.numberInWorld;
    }
    throw new Exception("Unable to find loot, probably an off-by-one error");
  }
}

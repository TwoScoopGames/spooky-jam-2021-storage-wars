using System.Collections.Generic;
using System.Linq;
using TMPro;
using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
  public LootGenerator lootGenerator;
  public Transform lootSpawnLocation;
  public Auction auction;
  public IntReference playersGoldPieces;
  public IntReference currentBid;
  public FloatReference totalWeightPounds;

  public TextMeshProUGUI bidder1Name;
  public TextMeshProUGUI bidder2Name;
  public TextMeshProUGUI bidder3Name;

  public TextMeshProUGUI bidder1Bid;
  public TextMeshProUGUI bidder2Bid;
  public TextMeshProUGUI bidder3Bid;

  public int minBidIncrement = 100;

  public TextMeshProUGUI bidButtonText;

  public GameObject goingOnce;
  public GameObject goingTwice;
  public GameObject sold;
  public TextMeshProUGUI soldText;
  public TextMeshProUGUI lootList;

  public GameObject overBid;
  public GameObject underBid;
  public GameObject gotAway;

  public UnityEvent eventsOnEnd;

  void Start() {
    auction = new Auction(lootGenerator, playersGoldPieces.Value);
    totalWeightPounds.Value = auction.totalWeightPounds;
    bidder1Name.text = auction.bidders[0].name;
    bidder2Name.text = auction.bidders[1].name;
    bidder3Name.text = auction.bidders[2].name;
    currentBid.Value = 0;

    var ll = "";
    foreach (var item in auction.items) {
      ll += item.BasicDescription + "\n";
    }
    lootList.text = ll;
  }

  private float secondsSinceLastBid;
  private float secondsUntilNextBid = 5f;
  private Bidder highBidder;
  private float secondsUntilCountdown = 10f;
  private int countdownStage = 0;

  void Update() {
    if (countdownStage == 3) {
      // FIXME: make restartable
      return;
    }

    secondsSinceLastBid += Time.deltaTime;
    if (secondsSinceLastBid > secondsUntilNextBid) {
      MakeBid();
    }
    if (countdownStage == 0 && secondsSinceLastBid > secondsUntilCountdown) {
      countdownStage++;
      goingOnce.gameObject.SetActive(true);
    }
    if (countdownStage == 1 && secondsSinceLastBid > secondsUntilCountdown * 2) {
      countdownStage++;
      goingTwice.gameObject.SetActive(true);
    }
    if (countdownStage == 2 && secondsSinceLastBid > secondsUntilCountdown * 3) {
      countdownStage++;
      var bidderName = "You";
      if (highBidder == null) {
        playersGoldPieces.Value -= currentBid.Value;
        playersGoldPieces.Value += auction.totalWorthGoldPieces;

      } else {
        bidderName = highBidder.name;
      }
      soldText.text = string.Format("Sold to {0} for {1} GP! The loot was worth {2} GP.", bidderName, currentBid.Value, auction.totalWorthGoldPieces);

      if (bidderName == "You"){
        if (currentBid.Value > auction.totalWorthGoldPieces){
          overBid.SetActive(true);
        }
        else{
          underBid.SetActive(true);
        }
      }else{
        gotAway.SetActive(true);
      }
      sold.SetActive(true);
      eventsOnEnd.Invoke();
      bidButtonText.transform.parent.gameObject.SetActive(false);
    }

    var nextBid = currentBid.Value + minBidIncrement;
    bidButtonText.text = string.Format("Bid {0} GP", currentBid.Value + minBidIncrement);
    var button = bidButtonText.transform.parent.gameObject.GetComponent<Button>();
    button.interactable = nextBid <= playersGoldPieces.Value;
  }

  public void PlayerBid() {
    highBidder = null;
    secondsSinceLastBid = 0;
    currentBid.Value = currentBid.Value + minBidIncrement;
    countdownStage = 0;
  }

  private void MakeBid() {
    var bidTexts = new[] {
      bidder1Bid, bidder2Bid, bidder3Bid
    };

    var bidderIndex = FindBidder();
    if (bidderIndex < 0) {
      return;
    }

    var bidderAmount = currentBid.Value + minBidIncrement;
    highBidder = auction.bidders[bidderIndex];
    bidTexts[bidderIndex].text = string.Format("I'll bid {0} GP!", bidderAmount);
    bidTexts[bidderIndex].gameObject.SetActive(true);
    currentBid.Value = bidderAmount;
    countdownStage = 0;

    secondsSinceLastBid = 0;
    secondsUntilNextBid = Random.Range(1, 10);
  }

  private int FindBidder() {
    var willingBidders = auction.bidders
      .Select((b, i) => i)
      .Where(i =>
          highBidder != auction.bidders[i] &&
          auction.bidders[i].willingToPayGoldPieces >= currentBid.Value + minBidIncrement)
      .ToArray();

    if (willingBidders.Length == 0) {
      return -1;
    }
    return Array.RandomElement(willingBidders);
  }

  public void SpawnLoot() {
    foreach (var item in auction.items) {
      Instantiate(item.prefab, lootSpawnLocation.transform.position, Quaternion.identity, lootSpawnLocation.transform.parent);
    }
  }
}

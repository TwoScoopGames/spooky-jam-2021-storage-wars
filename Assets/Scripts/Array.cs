using System.Collections.Generic;
using UnityEngine;

public class Array {
  public static T RandomElement<T>(T[] arr) {
    return arr[Random.Range(0, arr.Length)];
  }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Auction {
  public List<LootInstance> items;
  public Bidder[] bidders;

  public float totalVolumeCubicFeet = 0;
  public float totalWeightPounds = 0;
  public int totalWorthGoldPieces = 0;

  public Auction(LootGenerator lootGenerator, int playersGoldPieces) {
    items = lootGenerator.GenerateChest(50, 1000, 100000);

    foreach (var item in items) {
      totalVolumeCubicFeet += item.volumeCubicFeet;
      totalWeightPounds += item.weightPounds;
      totalWorthGoldPieces += item.worthGoldPieces;
    }

    bidders = new[] {
      Bidder.Generate(totalWorthGoldPieces, playersGoldPieces),
      Bidder.Generate(totalWorthGoldPieces, playersGoldPieces),
      Bidder.Generate(totalWorthGoldPieces, playersGoldPieces)
    };

    Debug.LogFormat("{0}", bidders[0].BasicDescription);
    Debug.LogFormat("{0}", bidders[1].BasicDescription);
    Debug.LogFormat("{0}", bidders[2].BasicDescription);
  }
}

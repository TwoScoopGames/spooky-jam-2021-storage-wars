using UnityEngine;
using System;

public class SineFloat : MonoBehaviour {
  Vector3 original;

  public float magnitudeY = 0;
  public float periodY = 1;

  public float magnitudeX = 0;
  public float periodX = 1;

   public float magnitudeZ = 0;
  public float periodZ = 1;


  void Start() {
    original = transform.position;
  }

  void Update() {
    var pos = transform.position;

    pos.y = original.y + ((float)Math.Sin(Time.time / periodY) * magnitudeY);
    pos.x = original.x + ((float)Math.Sin(Time.time / periodX) * magnitudeX);
    pos.z = original.z + ((float)Math.Sin(Time.time / periodZ) * magnitudeZ);


    transform.position = pos;
  }
}

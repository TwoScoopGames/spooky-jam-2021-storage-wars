using System.Collections.Generic;
using UnityEngine;

public class LootInstance {
  public string name;
  public GameObject prefab;
  public float volumeCubicFeet;
  public float weightPounds;
  public int worthGoldPieces;

  public string BasicDescription {
    get {
      return string.Format("{0} ({1} ft3 / {2} lbs / {3} gp)", name, volumeCubicFeet, weightPounds, worthGoldPieces);
    }
  }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RandomWord : MonoBehaviour{

    public TextMeshProUGUI text;
    public string[] randomWord;

    void Start(){
        text.text = string.Format(text.text, randomWord[Random.Range(0, randomWord.Length)]);
    }


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioFader : MonoBehaviour{
  public float defaultDuration = 1f;
  public AudioSource audioSource;
  public bool fadeInOnEnable = false;
  public float maxVolume = 1f;

  void OnEnable(){
    if (fadeInOnEnable){
      FadeIn();
    }
  }
  public void FadeOut(){
    StartCoroutine(StartFade(audioSource, defaultDuration, 0f));
  }

  public void FadeIn(){
    StartCoroutine(StartFade(audioSource, defaultDuration, maxVolume));
  }

  public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume){
    float currentTime = 0;
    float start = audioSource.volume;

    while (currentTime < duration){
      currentTime += Time.deltaTime;
      audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
      yield return null;
    }
    yield break;
  }

}

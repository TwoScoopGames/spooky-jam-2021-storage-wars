using UnityEngine;
using System.Collections;

public class RandomSoundsScript : MonoBehaviour {

    public AudioSource randomSound;

    public AudioClip[] audioSources;
    public bool playOnStart = true;

    // Use this for initialization
    void Start () {
      if(playOnStart){
        PlayRandomSound ();
      }
    }


    public void PlayRandomSound(){
        randomSound.clip = audioSources[Random.Range(0, audioSources.Length)];
        randomSound.Play ();
    }
}

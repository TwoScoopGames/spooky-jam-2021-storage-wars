using UnityEngine;
using UnityEngine.SceneManagement;


public class SwitchScene : MonoBehaviour {

  public bool SwitchSceneOnAwake;
  public string sceneName;


  public Animator animator;


  public void Start(){}

  public void OnEnable(){
    if(SwitchSceneOnAwake){
      Switch(sceneName);
    }
  }

  public void FadeOut(){
    animator.SetTrigger("FadeOut");
  }


  public void FadeIn(){
    animator.SetTrigger("fadeIn");
  }



  public void Switch(string scene) {
    Time.timeScale = 1f;
    sceneName = scene;
    animator.SetTrigger("FadeOut");
  }

  public void OnFadeComplete(){
    Time.timeScale = 1f;
    SceneManager.LoadScene(sceneName);
  }

  public void SwitchWithoutFade(string scene) {
    sceneName = scene;
    Time.timeScale = 1f;
    SceneManager.LoadScene(sceneName);
  }

  public void SwitchOnlyInEditor(string scene) {
    #if UNITY_EDITOR
        SwitchWithoutFade(scene);
    #endif
  }




}
